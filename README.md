# MLops homework

This is a homework repository for the [MLOps & production for data science research 3.0](https://ods.ai/tracks/mlops3-course-spring-2024).

## Contributing

At this moment, the project is being developed using the standard forking workflow (briefly described in `CONTRIBUTING.md`). This may be a subject to change as more data arrives.

## Code styling

As of now, automatic pre-commit checks include:

- mypy;
- ruff linter;
- ruff formatter.

Refer to `CONTRIBUTING.md` for the details.
