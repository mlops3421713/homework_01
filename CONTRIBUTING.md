# How to contribute

The preferred way to contribute is to fork the main repository on GitLab, then submit a “pull request” (PR).

In the first few steps, we explain how to locally install the project, and how to set up your git repository:

1. Create an account on GitLab if you do not already have one.

2. Fork the project repository: click on the ‘Fork’ button near the top of the page. This creates a copy of the code under your account on the GitLab user account.

3. Clone your fork of the repo from your GitLab account to your local disk:
```
git clone git@gitlab.com:YourLogin/homework_01.git # add --depth 1 if your connection is slow
cd homework_01
```
4. Recommended: create and activate a virtual environment for development:
```
python -m venv environment_name
source environment_name/bin/activate
#  When done with development, you can run `deactivate` to leave the environment.
```
5. Install the development dependencies:
```
pip install -r requirements.txt
```
6. Add the upstream remote. This saves a reference to the main repository, which you can use to keep your repository synchronized with the latest changes:

```
git remote add upstream git@gitlab.com:mlops3421713/homework_01.git
```
7. Check that the upstream and origin remote aliases are configured correctly by running git remote -v which should display:
```
origin  git@gitlab.com:YourLogin/homework_01.git (fetch)
origin  git@gitlab.com:YourLogin/homework_01.git (push)
upstream        git@gitlab.com:mlops3421713/homework_01.git (fetch)
upstream        git@gitlab.com:mlops3421713/homework_01.git (push)
```

You should now have your git repository properly configured.

The next steps now describe the process of modifying code and submitting a PR:

8. Synchronize your main branch with the upstream/main branch:
```
git checkout main
git fetch upstream
git merge upstream/main
```
9. Create a feature branch to hold your development changes and start making changes. Always use a feature branch. It’s good practice to never work on the main branch!
```
git checkout -b my_feature
```
10. Install pre-commit hooks to run code style checks before each commit (pre-commit checks can be disabled for a particular commit with `git commit -n`):
```
pre-commit install
```
11. Develop the feature on your feature branch on your computer, using Git to do the version control. When you’re done editing, add changed files using git add and then git commit to record your changes in Git:
```
git add modified_files
git commit
```
12. Push the changes to your GitLab account with:
```
git push -u origin my_feature
```

Follow [these](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html) instructions to create a pull request from your fork.

It is often helpful to keep your local feature branch synchronized with the latest changes of the main repository:
```
git fetch upstream
git merge upstream/main
```
Subsequently, you might need to solve the conflicts. You can refer to the [Git documentation related to resolving merge conflicts](https://docs.gitlab.com/ee/user/project/merge_requests/conflicts.html).
